import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import {HomePage} from "../home/home";


/**
 * Generated class for the SettingsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {
  cidade: string;
  temperatura: number;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private storage: Storage) {


    this.cidade = 'São Luís';
    this.temperatura = 34.33;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SettingsPage');
  }

  saveForm()
  {
    let location = {
      cidade: this.cidade,
      temperatura: this.temperatura,
    }

    this.navCtrl.push(HomePage);
  }

}
