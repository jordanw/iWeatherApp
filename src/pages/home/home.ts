import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { WeatherProvider } from '../../providers/weather/weather';
import { Storage } from "@ionic/storage";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {


  cidade: string;
  umidade: number;
  temperatura: number;
  visibilidade: number;
  precipitacao: number;
  img: string;
  clima: string;
  regiao: string;



  constructor(public navCtrl: NavController,
              public weatherProvider: WeatherProvider,
              private storage: Storage
               ) {
    }//private storage: Storage

  //this function has been called all time the view load

  ionViewWillEnter()
  {


    this.weatherProvider.getClima().subscribe(
      clima => {
        console.log(clima);
            this.cidade =  clima['location']['name'];
            this.regiao =  clima['location']['region'];
            this.umidade = clima['current']['humidity'];
            this.temperatura = clima['current']['temp_f'];
            this.visibilidade = clima['current']['vis_km'];
            this.img = clima['current']['condition']['icon'];
            this.precipitacao = clima['current']['precip_mm'];
            this.clima = clima['current']['condition']['text'];

        console.log(clima);
      });

  }


}
